# Sicredi teste técnico para dev backend. 
Proposta uma solução para votação de pautas.

## Como Executar o projeto
A unica dependência para executar o projeto será o uso do Docker. com docker instalado acessar a raiz do projeto e executar. 
```
docker-compose up -d
```

Assim que a aplicação subir a mesma pode ser acessada em: 

http://localhost:8087/api.sicredi.com.br/swagger-ui.html#/


O projeto pode ser executado diretamente com o Gradle.
```
./gradlew bootRun
```
## No que eu pensei?

Tentei focar mais no core da aplicação criando uma isolação entre o núcleo do negócio e as 
camadas de frameworks tentando preservar esse núcleo de dependências externas.

Separei a aplicação nas seguintes camadas;
- **_domain_** - Camada de domínio, que possui os objetos de negócio.
- **_service_** - Camada que possui as regras de negócio interagindo com os objetos de negócio.
- **_infrastructure_** - Camada que possui os accessos externos, aqui tive a intensão de manter todos os objetos e suas dependências com frameworks.
- **_presentation_** - Camada de apresentação que no nesse caso implementa apenas recursos REST.

## O que usei?
####  Java
Para esse projeto use o java 8, mas ele pode ser executado em versões superiores sem problemas.
#### Spring boot
Usei o Spring boot 2.1.5 com Gradle com a ajuda do projeto: https://start.spring.io/
#### Mysql 5.7
Não será necessário instalar o BD, toda config está no docker-compose.

## Operações básicas disponíveis
Aplicação pode ser acessada via swagger-ui:
```
http://localhost:8087/api.sicredi.com.br/swagger-ui.html#/
```

#### Adicionar nova pauta
```
POST contexto/pautas
```

#### Busca uma pauta
```
GET contexto/pautas/{id-pauta}
```

#### Iniciar nova sessão de votação em uma pauta
```
POST contexto/pautas/{id-pauta}/sessoes
```

#### Realiza uma votação em uma sessão de votação
*OBS: ao processar a tentativa de voto é realizada uma verificação do CPF atual com recurso externo.
se o mesmo for bloqueado para processar, tente novamente.
Sim, eu poderia ter adicionado um recurso para buscar CPFs válido de forma automática, mas não consegui finalizar tudo o que
eu gostaria e esse é um dos recusos que eu queria ter adicionado. Então, nesse momento
tenha calma e busque por CPFs válido byurself :-) SorryAndThaks!
```
POST contexto/{id-pauta}/sessoes/{id-sessao}/votos
```

#### Busca resultado da votação
```
GET contexto/{id-pauta}/sessoes/{id-sessao}/resultado
```

## O que eu não fiz e ficaria para uma evolução futura?
Deixo aqui algumas notas de quesitos que eu poderia melhorar ou aplicar;
- **_Adicionar testes de integração_**
- **_Melhorar a copertura de teste pois nesse caso foquei mais no componente de negócio e não implementei testes fora desse contexto_**
- **_Poderia modularizar o projeto_**
- **_Adicionar a dependência com kafka_**

