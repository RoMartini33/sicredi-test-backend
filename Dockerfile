FROM java:8

RUN mkdir /app

WORKDIR /app

COPY /build/libs/*.jar /app/app.jar

EXPOSE 8080

CMD ["java", "-jar", "-Xmx128M", "-Xms128M", "-Xss256k", "-XX:MaxMetaspaceSize=128M", "app.jar"]