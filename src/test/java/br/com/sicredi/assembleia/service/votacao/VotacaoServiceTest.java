package br.com.sicredi.assembleia.service.votacao;

import br.com.sicredi.assembleia.domain.ResultadoVotacao;
import br.com.sicredi.assembleia.domain.ResultadoVotacaoEnum;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.domain.VotoEnum;
import br.com.sicredi.assembleia.service.constraints.VoteAlreadyExistsException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static br.com.sicredi.assembleia.utils.MockUtils.*;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VotacaoServiceTest {

    public static final String VOTACAO_PARA_PAUTA_JA_REALIZADA = "Votação já realizada para essa pauta!";
    private VotacaoService service;

    @Mock
    private VotacaoDataProvider dataProvider;

    @Mock
    private VotacaoAdapter adapter;

    @Before
    public void setUp() {
        service = new VotacaoService(dataProvider, adapter);
    }

    @Test
    public void devePoderRealizarVotacaoPositivaParaUmaPautaEmVotacao() {
        Voto intesaoDeVoto = dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM);

        when(dataProvider.votar(any(Voto.class))).thenReturn(intesaoDeVoto);

        Voto votoRealizado = service.votar(intesaoDeVoto);

        assertNotNull(votoRealizado);
        assertTrue(votoRealizado.getEscolha() == VotoEnum.SIM);
        assertEquals(intesaoDeVoto, votoRealizado);
        verify(dataProvider, times(1)).votar(any(Voto.class));
    }

    @Test
    public void devePoderRealizarVotacaoContrariaParaUmaPautaEmVotacao() {
        Voto intesaoDeVoto = dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO);

        when(dataProvider.votar(any(Voto.class))).thenReturn(intesaoDeVoto);

        Voto votoRealizado = service.votar(intesaoDeVoto);

        assertNotNull(votoRealizado);
        assertTrue(votoRealizado.getEscolha() == VotoEnum.NAO);
        assertEquals(intesaoDeVoto, votoRealizado);
        verify(dataProvider, times(1)).votar(any(Voto.class));
    }

    @Test
    public void deveRetornarVerdadeiroSeAssociadoJaVotou() {
        when(dataProvider.votacaoExistente(any())).thenReturn(true);
        assertTrue(service.votacaoExistente(any(Voto.class)));
    }

    @Test
    public void deveRetornarFalsoSeAssociadoAindaNaoVotou() {
        when(dataProvider.votacaoExistente(any())).thenReturn(false);
        assertFalse(service.votacaoExistente(any(Voto.class)));
    }

    @Test
    public void deveRetornarExcecaoQuandoAssociadoJaVotouEmUmaPauta() {
        when(dataProvider.votacaoExistente(any())).thenReturn(true);

        assertThatThrownBy(() -> service.votar(any()))
                .isInstanceOf(VoteAlreadyExistsException.class)
                .hasMessageContaining(VOTACAO_PARA_PAUTA_JA_REALIZADA);
    }

    @Test
    public void deveContabilizarVotacoesNegativasParaPauta() {
        long totalVotosContratiosEsperado = 3;

        when(dataProvider.buscarVotacoes(anyLong(), anyLong()))
                .thenReturn(Optional.of(dadoQueExistamPautasEmVotacao()));

        long totalVotosContratios = service.quantidadeVotacoesParaPauta(anyLong(), anyLong(), VotoEnum.NAO);

        assertEquals(totalVotosContratiosEsperado, totalVotosContratios);
    }

    @Test
    public void deveContabilizarVotacoesPositivasParaPauta() {
        long totalVotosFavoraveisEsperado = 5;

        when(dataProvider.buscarVotacoes(anyLong(), anyLong()))
                .thenReturn(Optional.of(dadoQueExistamPautasEmVotacao()));

        long totalVotosFavoraveis = service.quantidadeVotacoesParaPauta(anyLong(), anyLong(), VotoEnum.SIM);

        assertEquals(totalVotosFavoraveisEsperado, totalVotosFavoraveis);
    }

    @Test
    public void resultadoDaVotacaoParaPautaDeveSertAprovada() {
        when(dataProvider.buscarVotacoes(anyLong(), anyLong()))
                .thenReturn(Optional.of(dadoQueExistamPautasEmVotacao()));

        ResultadoVotacao resultadoVotacao = service.resultadoVotacao(anyLong(), anyLong());

        assertNotNull(resultadoVotacao);
        assertEquals(5, resultadoVotacao.getTotalVotosFavoraveis());
        assertEquals(3, resultadoVotacao.getTotalVotosContrarios());
        assertTrue(ResultadoVotacaoEnum.APROVADA == resultadoVotacao.getSituacao());

    }

    @Test
    public void resultadoDaVotacaoParaPautaDeveSertRejeitada() {
        when(dataProvider.buscarVotacoes(anyLong(), anyLong()))
                .thenReturn(Optional.of(dadoQueExistamPautasEmVotacaoComMaisVotosContrarios()));

        ResultadoVotacao resultadoVotacao = service.resultadoVotacao(anyLong(), anyLong());

        assertNotNull(resultadoVotacao);
        assertEquals(2, resultadoVotacao.getTotalVotosFavoraveis());
        assertEquals(3, resultadoVotacao.getTotalVotosContrarios());
        assertTrue(ResultadoVotacaoEnum.REJEITADA == resultadoVotacao.getSituacao());

    }

    @Test
    public void resultadoDaVotacaoParaPautaDeveSertEmpate() {
        when(dataProvider.buscarVotacoes(anyLong(), anyLong()))
                .thenReturn(Optional.of(dadoQueExistamPautasEmVotacaoComVotosIguais()));

        ResultadoVotacao resultadoVotacao = service.resultadoVotacao(anyLong(), anyLong());

        assertNotNull(resultadoVotacao);
        assertEquals(2, resultadoVotacao.getTotalVotosFavoraveis());
        assertEquals(2, resultadoVotacao.getTotalVotosContrarios());
        assertTrue(ResultadoVotacaoEnum.EMPATE == resultadoVotacao.getSituacao());
    }

    @Test
    public void deveGerarExcecaoAoGeraraResultadoQuandoSessaoVotacaoNaoEstiverFinalizada() {
        when(dataProvider.buscarVotacoes(anyLong(), anyLong()))
                .thenReturn(Optional.of(dadoQueExistamPautasEmVotacaoComVotosIguais()));

        ResultadoVotacao resultadoVotacao = service.resultadoVotacao(anyLong(), anyLong());

        assertNotNull(resultadoVotacao);
        assertEquals(2, resultadoVotacao.getTotalVotosFavoraveis());
        assertEquals(2, resultadoVotacao.getTotalVotosContrarios());
        assertTrue(ResultadoVotacaoEnum.EMPATE == resultadoVotacao.getSituacao());
    }


}