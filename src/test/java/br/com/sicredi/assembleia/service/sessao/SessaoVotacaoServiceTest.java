package br.com.sicredi.assembleia.service.sessao;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.service.constraints.SessionAlreadyCloseException;
import br.com.sicredi.assembleia.service.pauta.PautaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static br.com.sicredi.assembleia.utils.MockUtils.*;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SessaoVotacaoServiceTest {
    private static final String SESSAO_DE_VOTACAO_JA_FINALIZADA = "Sessão de Votação já se enconta finalizada!";
    private SessaoVotacaoService service;

    @Mock
    private SessaoVotacaoDataProvider dataProvider;

    @Mock
    private FinalizacaoSessaoVotacaoAdapter adapter;

    @Mock
    private PautaService pautaService;

    @Before
    public void setUp() {
        service =  new SessaoVotacaoService(dataProvider, adapter, pautaService);
    }

    @Test
    public void deveAbrirNovaSessaoParaVotacaoEmUmaPautaComDuracaoPadraoDeUmMinuto() {
        Integer duracaoEsperada = 1;
        SessaoVotacao novaSessao = dadoQueTenhaIniciadaNovaSessao();

        when(dataProvider.iniciar(any(Pauta.class), any()))
                .thenReturn(novaSessao);

        when(pautaService.buscarPorCodigo(anyLong()))
                .thenReturn(Optional.of(dadoQueUmaPautaTenhaSidoCriada()));

        SessaoVotacao sessaoVotacaoAberta =
                service.iniciar(anyInt(), 0);

        assertNotNull(sessaoVotacaoAberta);
        assertEquals(sessaoVotacaoAberta, sessaoVotacaoAberta);
        assertNotNull(sessaoVotacaoAberta.getCodigo());
        assertEquals(sessaoVotacaoAberta.getDuracao(), duracaoEsperada);
        verify(dataProvider, times(1)).iniciar(any(Pauta.class), any());
    }

    @Test
    public void deveAbrirNovaSessaoParaVotacaoEmUmaPautaComDuracaoSendoInformada() {
        Integer duracaoEsperada = 40;
        SessaoVotacao novaSessao = dadoQueTenhaIniciadaNovaSessaoComDuracaoDe(duracaoEsperada);

        when(dataProvider.iniciar(any(Pauta.class), any()))
                .thenReturn(novaSessao);

        when(pautaService.buscarPorCodigo(anyLong()))
                .thenReturn(Optional.of(dadoQueUmaPautaTenhaSidoCriada()));

        SessaoVotacao sessaoVotacaoAberta =
                service.iniciar(anyInt(), duracaoEsperada);


        assertNotNull(sessaoVotacaoAberta);
        assertNotNull(sessaoVotacaoAberta.getCodigo());
        assertEquals(sessaoVotacaoAberta, sessaoVotacaoAberta);
        assertEquals(sessaoVotacaoAberta.getDuracao(), duracaoEsperada);
        assertEquals(LocalDateTime.parse("2019-06-10T10:50:00"), sessaoVotacaoAberta.getFechamento());
        verify(dataProvider, times(1)).iniciar(any(Pauta.class), any());
    }

    @Test
    public void deveFinalizarUmaSessaoAtivaNotificandoEncerramento() {
        SessaoVotacao sessaoExistente = dadoQueTenhaIniciadaNovaSessaoComDuracaoDe(2);

        doNothing().when(dataProvider).finalizarSessao(any());
        doNothing().when(adapter).notificarFinalizacaoDaSessao(any());

        service.finalizarSessao(sessaoExistente);

        verify(dataProvider, times(1)).finalizarSessao(any());
        verify(adapter, times(1)).notificarFinalizacaoDaSessao(any());
    }

    @Test
    public void deveLancarExcecaoSeTentarFecharSessaoJaFechada() {
        SessaoVotacao sessaoExistente = dadoQueTenhaIniciadaNovaSessaoComData(LocalDateTime.now().plusHours(1));

        assertThatThrownBy(() -> service.finalizarSessao(sessaoExistente))
                .isInstanceOf(SessionAlreadyCloseException.class)
                .hasMessageContaining(SESSAO_DE_VOTACAO_JA_FINALIZADA);

        verify(dataProvider, times(0)).finalizarSessao(any());
        verify(adapter, times(0)).notificarFinalizacaoDaSessao(any());
    }

    @Test
    public void deveRetornarColecaoDeSessoesDeVotacaoAtivas() {
        int totalElementosEsperados = 5;

        when(dataProvider.buscarSessoesAtivas())
                .thenReturn(Optional.of(dadoQueExistamSesseosDeVotacaoAtivas()));

        Collection sessoes = service.buscarSessoesAtivas().orElse(Collections.emptyList());

        assertNotNull(sessoes);
        assertEquals(totalElementosEsperados, sessoes.size());
        verify(dataProvider, times(1)).buscarSessoesAtivas();
    }

    @Test
    public void deveRetornarTodaSessoesDeUmaPauta() {
        when(dataProvider.buscarSessoesParaPauta(any()))
                .thenReturn(Optional.of(dadoQueExistamSesseosDeVotacaoAtivas()));

        Collection<SessaoVotacao> sessoes = service.buscarSessoesParaPauta(any())
                .orElse(Collections.emptyList());

        assertNotNull(sessoes);
        assertFalse(sessoes.isEmpty());
        assertEquals(5, sessoes.size());
    }

    @Test
    public void deveRetornarUmaSessaoPeloCodigo() {
        when(dataProvider.buscarSessaoPorCodigo(anyLong()))
                .thenReturn(Optional.of(dadoQueExistaUmaSessao()));

        SessaoVotacao sessao = service.buscarSessaoPorCodigo(anyInt())
                .orElse(null);

        assertNotNull(sessao);
        assertEquals(dadoQueExistaUmaSessao(), sessao);
    }

}