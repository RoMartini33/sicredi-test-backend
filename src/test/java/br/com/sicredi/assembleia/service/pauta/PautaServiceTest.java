package br.com.sicredi.assembleia.service.pauta;

import br.com.sicredi.assembleia.domain.Pauta;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static br.com.sicredi.assembleia.utils.MockUtils.dadoQueUmaPautaTenhaSidoCriada;
import static br.com.sicredi.assembleia.utils.MockUtils.dadoQueUmaPautaTenhaSidoCriadaComSessaoIniciada;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PautaServiceTest {

    private PautaService service;

    @Mock
    private PautaDataProvider dataProvider;

    @Before
    public void setUp() {
        service = new PautaService(dataProvider);
    }

    @Test
    public void deveRetornarNovaPautaQuandoCriadaSemSessaoDefinida() {
        Pauta novaPauta = dadoQueUmaPautaTenhaSidoCriada();

        when(dataProvider.adicionar(any(Pauta.class)))
                .thenReturn(novaPauta);

        Pauta pautaAdicionada = service.adicionar(novaPauta);

        verify(dataProvider, times(1)).adicionar(any(Pauta.class));
        assertNotNull(pautaAdicionada);
        assertEquals(novaPauta, pautaAdicionada);
    }

    @Test
    public void deveRetornarUmaPautaQuandoBuscaForPeloCodigo() {
        Optional<Pauta> pautaEsperada =
                Optional.of(dadoQueUmaPautaTenhaSidoCriada());

        when(dataProvider.buscarPorCodigo(anyLong()))
                .thenReturn(pautaEsperada);


        Optional<Pauta> pautaRetornada = service.buscarPorCodigo(anyLong());

        verify(dataProvider, times(1)).buscarPorCodigo(anyLong());
        assertNotNull(pautaRetornada);
        assertEquals(pautaEsperada, pautaRetornada);
    }

    @Test
    public void deveRetornarSessaoDeVotacaoAbertaParaUmaPautaQuandoBuscaForPeloCodigo() {
        Optional<Pauta> pautaEsperada = Optional
                .of(dadoQueUmaPautaTenhaSidoCriadaComSessaoIniciada());

        when(dataProvider.buscarPorCodigo(anyLong()))
                .thenReturn(pautaEsperada);

        Optional<Pauta> pautaRetornada = service.buscarPorCodigo(anyLong());

        verify(dataProvider, times(1)).buscarPorCodigo(anyLong());
        assertNotNull(pautaRetornada);
        assertEquals(pautaEsperada, pautaRetornada);
    }


}