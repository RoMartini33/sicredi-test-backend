package br.com.sicredi.assembleia.utils;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.domain.VotoEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;

public class MockUtils {

    public static final String ASSUNTO_DA_PAUTA = "Assunto da pauta";
    public static final String EMENTA_DA_PAUTA = "Ementa da pauta";
    public static final String DATE_2019_06_10_T_10_10_10 = "2019-06-10T10:10:00";
    public static final String CPF = "00280997028";

    public static Collection<SessaoVotacao> dadoQueExistamSesseosDeVotacaoAtivas() {
        return Arrays.asList(
                dadoQueTenhaIniciadaNovaSessaoComDuracaoDe(10),
                dadoQueTenhaIniciadaNovaSessaoComDuracaoDe(10),
                dadoQueTenhaIniciadaNovaSessaoComDuracaoDe(10),
                dadoQueTenhaIniciadaNovaSessao(),
                dadoQueTenhaIniciadaNovaSessao()
        );
    }

    public static SessaoVotacao dadoQueTenhaIniciadaNovaSessaoComDuracaoDe(int duracao) {
        SessaoVotacao sessao = dadoQueTenhaIniciadaNovaSessao();
        return SessaoVotacao.builder()
                .codigo(sessao.getCodigo())
                .abertura(sessao.getAbertura())
                .duracao(duracao)
                .build();
    }

    public static SessaoVotacao dadoQueTenhaIniciadaNovaSessaoComData(LocalDateTime data) {
        return SessaoVotacao.builder()
                .codigo(2L)
                .abertura(data)
                .build();
    }

    public static SessaoVotacao dadoQueTenhaIniciadaNovaSessao() {
        return SessaoVotacao.builder()
                .codigo(1L)
                .abertura(LocalDateTime.parse("2019-06-10T10:10:00"))
                .build();
    }

    public static Pauta dadoQueUmaPautaTenhaSidoCriada() {
        return Pauta.builder()
                .codigo(1L)
                .assunto(ASSUNTO_DA_PAUTA)
                .ementa(EMENTA_DA_PAUTA)
                .criacao(LocalDate.now())
                .build();
    }

    public static Pauta dadoQueUmaPautaTenhaSidoCriadaComSessaoIniciada() {
        Pauta pauta = dadoQueUmaPautaTenhaSidoCriada();
        return Pauta.builder()
                .codigo(pauta.getCodigo())
                .assunto(pauta.getAssunto())
                .ementa(pauta.getEmenta())
                .criacao(pauta.getDataCriacao())
                .build();
    }

    public static SessaoVotacao dadoQueExistaUmaSessao() {
        return SessaoVotacao.builder()
                .codigo(1L)
                .abertura(LocalDateTime.parse(DATE_2019_06_10_T_10_10_10))
                .build();
    }

    public static Collection<Voto> dadoQueExistamPautasEmVotacao() {
        return Arrays.asList(
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO)
        );
    }

    public static Collection<Voto> dadoQueExistamPautasEmVotacaoComMaisVotosContrarios() {
        return Arrays.asList(
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO)
        );
    }

    public static Collection<Voto> dadoQueExistamPautasEmVotacaoComVotosIguais() {
        return Arrays.asList(
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.SIM),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO),
                dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum.NAO)
        );
    }

    public static Voto dadoQueAssociadoTenhaManifestadoSuaIntecaoParaVoto(VotoEnum opcao) {
        Voto mock = new Voto();
        mock.setCodigo(1L);
        mock.setCpf(CPF);
        mock.setPauta(dadoQueUmaPautaTenhaSidoCriada());
        mock.setEscolha(opcao);
        return mock;
    }

}
