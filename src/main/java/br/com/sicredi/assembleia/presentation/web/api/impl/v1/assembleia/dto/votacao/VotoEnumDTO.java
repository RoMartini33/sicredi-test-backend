package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao;

import java.io.Serializable;
import java.util.Arrays;

public enum VotoEnumDTO implements Serializable {
    SIM (true),
    NAO (false);

    private final String UNSUPPORTED_TYPE_MESSAGE = "Opção de voto não suportada!";
    private boolean opcao;

    VotoEnumDTO(boolean opcao) {
        this.opcao = opcao;
    }

    public VotoEnumDTO getByValue(final boolean opcao) {
        return Arrays.stream(VotoEnumDTO.values())
                .filter(type -> type.equals(opcao))
                .findFirst()
                .orElseThrow(() ->
                        new IllegalStateException(String.format(UNSUPPORTED_TYPE_MESSAGE, opcao)));
    }

}
