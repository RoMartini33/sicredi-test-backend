package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.port.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "cpfValidate", url = "https://user-info.herokuapp.com/")
public interface CpfVotacaoResult {

    @RequestMapping(method = RequestMethod.GET, value = "/users/{cpf}", produces = "application/json")
    String getCpf(@PathVariable("cpf") String cpf);
}
