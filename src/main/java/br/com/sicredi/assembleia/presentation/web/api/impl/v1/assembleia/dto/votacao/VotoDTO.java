package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao;

import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.sessao.SessaoVotacaoDTO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class VotoDTO implements Serializable {
    private Long codigo;
    private String cpf;
    private PautaDTO pauta;
    private boolean escolha;
    private SessaoVotacaoDTO sessao;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public PautaDTO getPauta() {
        return pauta;
    }

    public void setPauta(PautaDTO pauta) {
        this.pauta = pauta;
    }

    public boolean isEscolha() {
        return escolha;
    }

    public void setEscolha(boolean escolha) {
        this.escolha = escolha;
    }

    public SessaoVotacaoDTO getSessao() {
        return sessao;
    }

    public void setSessao(SessaoVotacaoDTO sessao) {
        this.sessao = sessao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VotoDTO votoDTO = (VotoDTO) o;

        return new EqualsBuilder()
                .append(escolha, votoDTO.escolha)
                .append(codigo, votoDTO.codigo)
                .append(cpf, votoDTO.cpf)
                .append(pauta, votoDTO.pauta)
                .append(sessao, votoDTO.sessao)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(cpf)
                .append(pauta)
                .append(escolha)
                .append(sessao)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("cpf", cpf)
                .append("pauta", pauta)
                .append("escolha", escolha)
                .append("sessao", sessao)
                .toString();
    }
}
