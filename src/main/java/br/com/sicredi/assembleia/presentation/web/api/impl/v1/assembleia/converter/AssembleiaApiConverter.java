package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.converter;

import br.com.sicredi.assembleia.domain.*;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaCriarDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.sessao.SessaoVotacaoDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.ResultadoVotacaoDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.ResultadoVotacaoEnumDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.VotoCriarDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.VotoDTO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class AssembleiaApiConverter {

    public Pauta toBO(PautaCriarDTO dto) {
        return Pauta.builder()
                .ementa(dto.getEmenta())
                .assunto(dto.getAssunto())
                .build();
    }

    public PautaDTO toDTO(Pauta bo) {
        if (bo == null) return null;
        PautaDTO dto = new PautaDTO();

        dto.setCodigo(bo.getCodigo());
        dto.setAssunto(bo.getAssunto());
        dto.setEmenta(bo.getEmenta());
        dto.setDataCriacao(bo.getDataCriacao());

        return dto;
    }

    public SessaoVotacaoDTO toDTO(SessaoVotacao bo) {
        if (bo == null) return null;
        SessaoVotacaoDTO dto = new SessaoVotacaoDTO();

        dto.setCodigo(bo.getCodigo());
        dto.setAbertura(bo.getAbertura());
        dto.setFechamento(bo.getFechamento());
        dto.setDuracao(bo.getDuracao());
        dto.setAtiva(bo.situacao());

        return dto;
    }

    public Voto toBO(VotoCriarDTO dto) {
        if (dto == null) return null;
        Voto bo = new Voto();

        bo.setCpf(StringUtils.trim(dto.getCpf()));
        bo.setEscolha(VotoEnum.getByValue(dto.isEscolha()));

        return bo;
    }

    public VotoDTO toDTO(Voto bo) {
        if (bo == null) return null;
        VotoDTO dto = new VotoDTO();

        dto.setCodigo(bo.getCodigo());
        dto.setCpf(bo.getCpf());
        dto.setEscolha(bo.getEscolha().getOpcao());
        dto.setPauta(toDTO(bo.getPauta()));
        dto.setSessao(toDTO(bo.getSessao()));

        return dto;
    }

    public ResultadoVotacaoDTO toDTO(ResultadoVotacao rv) {
        if (rv == null) return null;
        ResultadoVotacaoDTO rvd = new ResultadoVotacaoDTO();

        rvd.setVotosFavoraveis(rv.getTotalVotosFavoraveis());
        rvd.setVotosContrarios(rv.getTotalVotosContrarios());
        rvd.setSituacao(toDTO(rv.getSituacao()));
        rvd.setCodigoPauta(rv.getCodigoPauta());
        rvd.setCodigoSessao(rv.getCodigoSessao());

        return rvd;
    }

    private ResultadoVotacaoEnumDTO toDTO(ResultadoVotacaoEnum rve) {
        if (rve == null) return null;
        return ResultadoVotacaoEnumDTO.valueOf(rve.name());
    }

}
