package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia;

import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaCriarDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.sessao.SessaoVotacaoDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.ResultadoVotacaoDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.VotoCriarDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.VotoDTO;
import org.springframework.http.ResponseEntity;

public interface AssembleiaApi {

    ResponseEntity<PautaDTO> criarPauta(PautaCriarDTO pauta);

    ResponseEntity<PautaDTO> buscarPautaPorCodigo(int pautaId);

    ResponseEntity<SessaoVotacaoDTO> abrirSessao(int pautaId, int duracaoSessao);

    ResponseEntity<VotoDTO> votar(int pautaId, int sessaoId, VotoCriarDTO voto);

    ResponseEntity<ResultadoVotacaoDTO> resultado(int pautaId, int sessaoId);

}
