package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta;

import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.sessao.SessaoVotacaoDTO;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.time.LocalDate;

public class PautaDTO implements Serializable {
    private Long codigo;
    private String assunto;
    private String ementa;
    private LocalDate dataCriacao;
    private SessaoVotacaoDTO sessaoVotacao;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public SessaoVotacaoDTO getSessaoVotacao() {
        return sessaoVotacao;
    }

    public void setSessaoVotacao(SessaoVotacaoDTO sessaoVotacao) {
        this.sessaoVotacao = sessaoVotacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PautaDTO pautaDTO = (PautaDTO) o;

        return new EqualsBuilder()
                .append(codigo, pautaDTO.codigo)
                .append(assunto, pautaDTO.assunto)
                .append(ementa, pautaDTO.ementa)
                .append(dataCriacao, pautaDTO.dataCriacao)
                .append(sessaoVotacao, pautaDTO.sessaoVotacao)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(assunto)
                .append(ementa)
                .append(dataCriacao)
                .append(sessaoVotacao)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("assunto", assunto)
                .append("ementa", ementa)
                .append("dataCriacao", dataCriacao)
                .append("sessaoVotacao", sessaoVotacao)
                .toString();
    }
}
