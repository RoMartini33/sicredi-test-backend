package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.sessao;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

public class SessaoVotacaoDTO implements Serializable {
    private Long codigo;
    private LocalDateTime abertura;
    private LocalDateTime fechamento;
    private Integer duracao;
    private Boolean ativa;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public LocalDateTime getAbertura() {
        return abertura;
    }

    public void setAbertura(LocalDateTime abertura) {
        this.abertura = abertura;
    }

    public LocalDateTime getFechamento() {
        return fechamento;
    }

    public void setFechamento(LocalDateTime fechamento) {
        this.fechamento = fechamento;
    }

    public Integer getDuracao() {
        return duracao;
    }

    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SessaoVotacaoDTO that = (SessaoVotacaoDTO) o;

        return new EqualsBuilder()
                .append(codigo, that.codigo)
                .append(abertura, that.abertura)
                .append(fechamento, that.fechamento)
                .append(duracao, that.duracao)
                .append(ativa, that.ativa)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(abertura)
                .append(fechamento)
                .append(duracao)
                .append(ativa)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("abertura", abertura)
                .append("fechamento", fechamento)
                .append("duracao", duracao)
                .append("ativa", ativa)
                .toString();
    }
}
