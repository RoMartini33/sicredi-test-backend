package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class VotoCriarDTO implements Serializable {
    private String cpf;
    private boolean escolha;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public boolean isEscolha() {
        return escolha;
    }

    public void setEscolha(boolean escolha) {
        this.escolha = escolha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VotoCriarDTO that = (VotoCriarDTO) o;

        return new EqualsBuilder()
                .append(escolha, that.escolha)
                .append(cpf, that.cpf)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(cpf)
                .append(escolha)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("cpf", cpf)
                .append("escolha", escolha)
                .toString();
    }
}
