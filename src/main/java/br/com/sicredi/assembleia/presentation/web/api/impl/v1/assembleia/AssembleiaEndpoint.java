package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia;


import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.converter.AssembleiaApiConverter;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaCriarDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.pauta.PautaDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.sessao.SessaoVotacaoDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.ResultadoVotacaoDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.VotoCriarDTO;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao.VotoDTO;
import br.com.sicredi.assembleia.service.AssembleiaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(value = "AssembleiaVotacao")
@RequestMapping(value = "/assembleia/v1/pautas")
public class AssembleiaEndpoint implements AssembleiaApi {
    private AssembleiaService service;
    private AssembleiaApiConverter converter;

    @Autowired
    public AssembleiaEndpoint(AssembleiaService service, AssembleiaApiConverter converter) {
        this.service = service;
        this.converter = converter;
    }

    @Override
    @PostMapping
    @ApiOperation(value = "Abrir uma pauta")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Pauta aberta com sucesso") })
    public ResponseEntity<PautaDTO> criarPauta(@RequestBody PautaCriarDTO pauta) {
        Pauta pautaCriada = service.criarPauta(converter.toBO(pauta));
        return new ResponseEntity<>(converter.toDTO(pautaCriada), HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/{id-pauta}")
    @ApiOperation(value = "Buscar uma pauta")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Pauta encontarada") })
    public ResponseEntity<PautaDTO> buscarPautaPorCodigo(@PathVariable("id-pauta") int pautaId) {
        return service.buscarPautaPorCodigo(pautaId)
                .map(p -> new ResponseEntity<>(converter.toDTO(p), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @Override
    @PostMapping("/{id-pauta}/sessoes")
    @ApiOperation(value = "Iniciar uma sessão")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Sessão iniciada com sucesso") })
    public ResponseEntity<SessaoVotacaoDTO> abrirSessao(@PathVariable("id-pauta") int pautaId,
                                                        @RequestParam(defaultValue = "1") int duracaoSessao) {
        SessaoVotacao sessaoIniciada = service.abrirSessao(pautaId, duracaoSessao);
        return new ResponseEntity<>(converter.toDTO(sessaoIniciada), HttpStatus.CREATED);
    }

    @Override
    @PostMapping("/{id-pauta}/sessoes/{id-sessao}/votos")
    @ApiOperation(value = "Votar em uma pauta")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Voto informado com sucesso") })
    public ResponseEntity<VotoDTO> votar(@PathVariable("id-pauta") int pautaId,
                                         @PathVariable("id-sessao") int sessaoId,
                                         @RequestBody VotoCriarDTO voto) {
         Voto votoRealizado = service.votar(pautaId, sessaoId, converter.toBO(voto));
        return new ResponseEntity<>(converter.toDTO(votoRealizado), HttpStatus.CREATED);
    }

    @Override
    @GetMapping("/{id-pauta}/sessoes/{id-sessao}/resultado")
    @ApiOperation(value = "Resultado da votação")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Votação finalizada") })
    public ResponseEntity<ResultadoVotacaoDTO> resultado(@PathVariable("id-pauta") int pautaId,
                                                         @PathVariable("id-sessao") int sessaoId) {
        return service.buscarResultadoVotacao(pautaId, sessaoId)
                .map(p -> new ResponseEntity<>(converter.toDTO(p), HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
