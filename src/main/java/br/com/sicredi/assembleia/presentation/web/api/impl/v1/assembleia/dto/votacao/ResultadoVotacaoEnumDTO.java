package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao;

import java.io.Serializable;

public enum ResultadoVotacaoEnumDTO implements Serializable {
    APROVADA,
    REJEITADA,
    EMPATE
}
