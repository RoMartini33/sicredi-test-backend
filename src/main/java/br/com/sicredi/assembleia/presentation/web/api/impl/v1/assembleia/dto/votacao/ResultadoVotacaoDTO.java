package br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.dto.votacao;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class ResultadoVotacaoDTO implements Serializable {
    private long votosFavoraveis;
    private long votosContrarios;
    private long codigoSessao;
    private long codigoPauta;
    private ResultadoVotacaoEnumDTO situacao;

    public long getVotosFavoraveis() {
        return votosFavoraveis;
    }

    public void setVotosFavoraveis(long votosFavoraveis) {
        this.votosFavoraveis = votosFavoraveis;
    }

    public long getVotosContrarios() {
        return votosContrarios;
    }

    public void setVotosContrarios(long votosContrarios) {
        this.votosContrarios = votosContrarios;
    }

    public long getCodigoSessao() {
        return codigoSessao;
    }

    public void setCodigoSessao(long codigoSessao) {
        this.codigoSessao = codigoSessao;
    }

    public long getCodigoPauta() {
        return codigoPauta;
    }

    public void setCodigoPauta(long codigoPauta) {
        this.codigoPauta = codigoPauta;
    }

    public ResultadoVotacaoEnumDTO getSituacao() {
        return situacao;
    }

    public void setSituacao(ResultadoVotacaoEnumDTO situacao) {
        this.situacao = situacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ResultadoVotacaoDTO that = (ResultadoVotacaoDTO) o;

        return new EqualsBuilder()
                .append(votosFavoraveis, that.votosFavoraveis)
                .append(votosContrarios, that.votosContrarios)
                .append(codigoSessao, that.codigoSessao)
                .append(codigoPauta, that.codigoPauta)
                .append(situacao, that.situacao)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(votosFavoraveis)
                .append(votosContrarios)
                .append(codigoSessao)
                .append(codigoPauta)
                .append(situacao)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("votosFavoraveis", votosFavoraveis)
                .append("votosContrarios", votosContrarios)
                .append("codigoSessao", codigoSessao)
                .append("codigoPauta", codigoPauta)
                .append("situacao", situacao)
                .toString();
    }
}
