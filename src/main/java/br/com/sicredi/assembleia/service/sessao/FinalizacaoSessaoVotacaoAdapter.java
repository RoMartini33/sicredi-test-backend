package br.com.sicredi.assembleia.service.sessao;

import br.com.sicredi.assembleia.domain.SessaoVotacao;

public interface FinalizacaoSessaoVotacaoAdapter {
    void notificarFinalizacaoDaSessao(SessaoVotacao sessao);
}
