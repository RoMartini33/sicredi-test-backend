package br.com.sicredi.assembleia.service.votacao;

import br.com.sicredi.assembleia.domain.ResultadoVotacao;

public interface VotacaoAdapter {
    void notificar(ResultadoVotacao resultado);
}
