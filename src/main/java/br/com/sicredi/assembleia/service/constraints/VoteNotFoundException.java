package br.com.sicredi.assembleia.service.constraints;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class VoteNotFoundException extends RuntimeException {
    private static final String VOTE_NOT_FOUND_EXCEPTION_MESSAGE
            = "Voto inexistente para pauta e sessão!";

    public VoteNotFoundException() {
        super(VOTE_NOT_FOUND_EXCEPTION_MESSAGE);
    }
}
