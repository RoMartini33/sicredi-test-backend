package br.com.sicredi.assembleia.service;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.ResultadoVotacao;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.presentation.web.api.impl.v1.assembleia.port.http.CpfVotacaoResult;
import br.com.sicredi.assembleia.service.constraints.BusinessException;
import br.com.sicredi.assembleia.service.pauta.PautaService;
import br.com.sicredi.assembleia.service.sessao.SessaoVotacaoService;
import br.com.sicredi.assembleia.service.votacao.VotacaoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AssembleiaService {
    public static final String VOTO_INEXISTENTE = "Voto inexistente!";
    public static final String PAUTA_INEXISTENTE = "Pauta inexistente!";
    public static final String SESSAO_INEXISTENTE = "Sessão inexistente!";
    public static final String SESSAO_FINALIZADA = "Tentativa de realizar votação em pauta :id {} com sessão :id {} finalizada";
    public static final String SESSAO_FECHADA_PARA_VOTACAO = "Sessão fechada para votação!";
    public static final String CPF_NAO_PERMITIDO_PARA_VOTO_NO_MOMENTO = "CPF não permitido para voto no momento, tente novemente!";
    private final Logger logger = LoggerFactory.getLogger(AssembleiaService.class);

    private PautaService pautaService;
    private SessaoVotacaoService sessaoService;
    private VotacaoService votacaoService;
    private CpfVotacaoResult cpfValidator;
    @Autowired
    public AssembleiaService(PautaService pautaService, SessaoVotacaoService sessaoService,
                             VotacaoService votacaoService, CpfVotacaoResult cpfValidator) {
        this.pautaService = pautaService;
        this.sessaoService = sessaoService;
        this.votacaoService = votacaoService;
        this.cpfValidator = cpfValidator;
    }

    public Pauta criarPauta(final Pauta pauta) {
        return pautaService.adicionar(pauta);
    }

    public Optional<Pauta> buscarPautaPorCodigo(long codigo) {
        return pautaService.buscarPorCodigo(codigo);
    }

    public SessaoVotacao abrirSessao(final int pautaId, final int duracaoSessao) {
        return sessaoService.iniciar(pautaId, duracaoSessao);
    }

    public Voto votar(final int pautaId, final int sessaoId, final Voto voto) {
        String cpfResult = cpfValidator.getCpf(voto.getCpf());

        if (StringUtils.trim(cpfResult).contains("UNABLE_TO_VOTE")) {
            logger.error("CPF não permitido para voto {} - {}", voto.getCpf(), cpfResult);
            throw new BusinessException(CPF_NAO_PERMITIDO_PARA_VOTO_NO_MOMENTO);
        }
        return votacaoService.votar(prepararVotacao(voto, pautaId, sessaoId));
    }

    private Voto prepararVotacao(Voto voto, int pautaId, int sessaoId) {
        if (voto == null) {
            throw new BusinessException(VOTO_INEXISTENTE);
        }

        Pauta pauta = pautaService.buscarPorCodigo(pautaId)
                .orElseThrow(() -> new BusinessException(PAUTA_INEXISTENTE));

        SessaoVotacao sessaoEmVotacao = sessaoService.buscarSessaoPorCodigo(sessaoId)
                .orElseThrow(() -> new BusinessException(SESSAO_INEXISTENTE));

        if (sessaoEstaAberta(sessaoEmVotacao)) {
            return prepararVoto(voto, pauta, sessaoEmVotacao);
        } else {
            logger.warn(SESSAO_FINALIZADA, pautaId, sessaoId);
            throw new BusinessException(SESSAO_FECHADA_PARA_VOTACAO);
        }

    }

    private Voto prepararVoto(Voto voto, Pauta pauta, SessaoVotacao sessaoEmVotacao) {
        Pauta pautaEmVotacao = Pauta.builder()
                .codigo(pauta.getCodigo())
                .criacao(pauta.getDataCriacao())
                .assunto(pauta.getAssunto())
                .ementa(pauta.getEmenta())
                .build();

        voto.setPauta(pautaEmVotacao);
        voto.setSessao(sessaoEmVotacao);
        return voto;
    }

    private boolean sessaoEstaAberta(SessaoVotacao sv) {
        return sv.situacao();
    }

    public Optional<ResultadoVotacao> buscarResultadoVotacao(int pautaId, int sessaoId) {
        return Optional.ofNullable(votacaoService.resultadoVotacao(pautaId, sessaoId));
    }

}
