package br.com.sicredi.assembleia.service.votacao;

import br.com.sicredi.assembleia.domain.ResultadoVotacao;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.domain.VotoEnum;

public interface VotacaoBoundary {

    Voto votar(Voto voto);

    boolean votacaoExistente(Voto voto);

    long quantidadeVotacoesParaPauta(long codigoPauta, long codigoSessao, VotoEnum filtro);

    ResultadoVotacao resultadoVotacao(long codigoPauta, long codigoSessao);

    void gerarResultadoParaVotacao(SessaoVotacao sessao);
}
