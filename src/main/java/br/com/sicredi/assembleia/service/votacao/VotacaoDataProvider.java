package br.com.sicredi.assembleia.service.votacao;

import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface VotacaoDataProvider {

    Voto votar(Voto voto);

    boolean votacaoExistente(Voto voto);

    Optional<Collection<Voto>> buscarVotacoes(long codigoPauta, long codigoSessao);

    Optional<Collection<Voto>> buscarPorCodigoSessao(SessaoVotacao sessaoVotacao);

    Optional<Voto> buscarPorCodigo(long codigoSessao);
}
