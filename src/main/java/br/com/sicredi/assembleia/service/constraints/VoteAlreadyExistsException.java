package br.com.sicredi.assembleia.service.constraints;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class VoteAlreadyExistsException extends RuntimeException {
    private static final String VOTE_ALREADY_EXISTS_EXCEPTION_MESSAGE
            = "Votação já realizada para essa pauta!";

    public VoteAlreadyExistsException() {
        super(VOTE_ALREADY_EXISTS_EXCEPTION_MESSAGE);
    }
}
