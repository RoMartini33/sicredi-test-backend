package br.com.sicredi.assembleia.service.sessao;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.service.constraints.BusinessException;
import br.com.sicredi.assembleia.service.pauta.PautaService;
import br.com.sicredi.assembleia.service.constraints.SessionAlreadyCloseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
public class SessaoVotacaoService implements SessaoVotacaoBoundary {
    public static final String SESSAO_SENDO_FINALIZADA = "Sessão sendo finalizada:{}";
    Logger logger = LoggerFactory.getLogger(SessaoVotacaoService.class);

    private static final String SESSAO_JA_FECHADA_LOG_MESSAGE
            = "Tentativa de realização de fechamento de sessão de votação já fechada";
    public static final String PAUTA_INEXISTENTE = "Pauta inexistente!";

    private final SessaoVotacaoDataProvider dataProvider;
    private final FinalizacaoSessaoVotacaoAdapter adapter;
    private final PautaService pautaService;

    @Autowired
    public SessaoVotacaoService(SessaoVotacaoDataProvider dataProvider, FinalizacaoSessaoVotacaoAdapter adapter, PautaService pautaService) {
        this.dataProvider = dataProvider;
        this.adapter = adapter;
        this.pautaService = pautaService;
    }

    @Override
    public SessaoVotacao iniciar(int pautaId, int duracao) {
        Pauta pauta = pautaService.buscarPorCodigo(pautaId)
                .orElseThrow(() -> new BusinessException(PAUTA_INEXISTENTE));

        SessaoVotacao sv = SessaoVotacao.builder()
                .abertura(LocalDateTime.now())
                .duracao(duracao)
                .situacao(true)
                .build();

        return dataProvider.iniciar(pauta, sv);
   }

    @Override
    public void finalizarSessao(SessaoVotacao sessao) {
        if (sessaoInativa(sessao.getFechamento())) {
            logger.warn(SESSAO_JA_FECHADA_LOG_MESSAGE, sessao);
            throw new SessionAlreadyCloseException();
        }
        logger.info(SESSAO_SENDO_FINALIZADA, sessao);
        dataProvider.finalizarSessao(sessao);
        adapter.notificarFinalizacaoDaSessao(sessao);
    }

    @Override
    public Optional<Collection<SessaoVotacao>> buscarSessoesAtivas() {
        return dataProvider.buscarSessoesAtivas();
    }

    @Override
    public Optional<Collection<SessaoVotacao>> buscarSessoesParaPauta(Pauta pauta) {
        return dataProvider.buscarSessoesParaPauta(pauta);
    }

    @Override
    public Optional<SessaoVotacao> buscarSessaoPorCodigo(int codigo) {
        return dataProvider.buscarSessaoPorCodigo(codigo);
    }

    private boolean sessaoInativa(LocalDateTime fechamento) {
        LocalDateTime now = LocalDateTime.now();
        return now.isBefore(fechamento);
    }

    @Scheduled(cron = "${jobs.sessao-votacao.fechar.cron}")
    public void finalizarSessaoJobImpl() {
        Collection<SessaoVotacao> sessoesAtivas = dataProvider.buscarSessoesAtivas().orElse(null);

        if (sessoesAtivas != null) {
            for (SessaoVotacao sv : sessoesAtivas) {
                if (sv.getFechamento().isBefore(LocalDateTime.now())) {
                    finalizarSessao(sv);
                }
            }
        }
    }

}
