package br.com.sicredi.assembleia.service.pauta;

import br.com.sicredi.assembleia.domain.Pauta;

import java.util.Optional;

public interface PautaDataProvider {

    Pauta adicionar(Pauta pauta);

    Optional<Pauta> buscarPorCodigo(long codigo);

}
