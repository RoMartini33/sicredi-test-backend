package br.com.sicredi.assembleia.service.pauta;

import br.com.sicredi.assembleia.domain.Pauta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PautaService implements PautaBoundary {
    private final PautaDataProvider dataProvider;

    @Autowired
    public PautaService(PautaDataProvider dataProvider) {
        this.dataProvider = dataProvider;
    }

    @Override
    public Pauta adicionar(Pauta pauta) {
        return dataProvider.adicionar(pauta);
    }

    @Override
    public Optional<Pauta> buscarPorCodigo(long codigo) {
        return dataProvider.buscarPorCodigo(codigo);
    }

}
