package br.com.sicredi.assembleia.service.constraints;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class SessionAlreadyCloseException extends RuntimeException {
    private static final String SESSION_ALREADY_CLOSE_EXCPETION_MESSAGE
            = "Sessão de Votação já se enconta finalizada!";

    public SessionAlreadyCloseException() {
        super(SESSION_ALREADY_CLOSE_EXCPETION_MESSAGE);
    }
}
