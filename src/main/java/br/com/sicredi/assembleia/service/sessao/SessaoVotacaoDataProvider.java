package br.com.sicredi.assembleia.service.sessao;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;

import java.util.Collection;
import java.util.Optional;

public interface SessaoVotacaoDataProvider {

    SessaoVotacao iniciar(Pauta pauta, SessaoVotacao sessaoVotacao);

    void finalizarSessao(SessaoVotacao sessao);

    Optional<Collection<SessaoVotacao>> buscarSessoesAtivas();

    Optional<Collection<SessaoVotacao>> buscarSessoesParaPauta(Pauta pauta);

    Optional<SessaoVotacao> buscarSessaoPorCodigo(long codigo);
}
