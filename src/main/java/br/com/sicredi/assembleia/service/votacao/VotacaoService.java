package br.com.sicredi.assembleia.service.votacao;


import br.com.sicredi.assembleia.domain.ResultadoVotacao;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.domain.VotoEnum;
import br.com.sicredi.assembleia.service.constraints.VoteAlreadyExistsException;
import br.com.sicredi.assembleia.service.constraints.VoteNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

@Service
public class VotacaoService implements VotacaoBoundary {
    public static final String TENTATIVA_DE_GERACAO_DE_RESULTADO_PARA_SESSAO_NAO_EXISTENTE = "Tentativa de geração de resultado para sessão não existente";
    Logger logger = LoggerFactory.getLogger(VotacaoService.class);

    public static final String PAUTA_JA_VOTADA_LOG_MESSAGE = "Tentativa de realização de voto em pauta já votada ";

    private final VotacaoDataProvider dataProvider;
    private final VotacaoAdapter adapter;

    @Autowired
    public VotacaoService(VotacaoDataProvider dataProvider, VotacaoAdapter adapter) {
        this.dataProvider = dataProvider;
        this.adapter = adapter;
    }

    @Override
    public Voto votar(Voto voto) {
        if (votacaoExistente(voto)) {
            logger.warn(PAUTA_JA_VOTADA_LOG_MESSAGE, voto);
            throw new VoteAlreadyExistsException();
        }
        return dataProvider.votar(voto);
    }

    @Override
    public boolean votacaoExistente(Voto voto) {
        return dataProvider.votacaoExistente(voto);
    }

    @Override
    public long quantidadeVotacoesParaPauta(long codigoPauta, long codigoSessao, VotoEnum filtro) {
        Collection<Voto> votos = dataProvider.buscarVotacoes(codigoPauta, codigoSessao)
                .orElseThrow(() -> new VoteNotFoundException());

        return votos.stream().filter(v -> v.getEscolha() == filtro).count();
    }

    @Override
    public ResultadoVotacao resultadoVotacao(long codigoPauta, long codigoSessao) {
        long contrarios = quantidadeVotacoesParaPauta(codigoPauta, codigoSessao, VotoEnum.NAO);
        long favoravies = quantidadeVotacoesParaPauta(codigoPauta, codigoSessao,  VotoEnum.SIM);

        return ResultadoVotacao.builder()
                .votosFavoraveis(favoravies)
                .votosContrarios(contrarios)
                .codigoPauta(codigoPauta)
                .codigoSessao(codigoSessao)
                .build();
    }


    @Override
    public void gerarResultadoParaVotacao(SessaoVotacao sessao) {
        Collection<Voto> votos = dataProvider.buscarPorCodigoSessao(sessao).orElse(null);

        if (CollectionUtils.isEmpty(votos)) {
            logger.warn(TENTATIVA_DE_GERACAO_DE_RESULTADO_PARA_SESSAO_NAO_EXISTENTE);
        } else {
            Voto voto = votos.stream().findFirst().get();
            ResultadoVotacao resultado =
                    resultadoVotacao(voto.getPauta().getCodigo(), voto.getSessao().getCodigo());
            adapter.notificar(resultado);
        }
    }

}
