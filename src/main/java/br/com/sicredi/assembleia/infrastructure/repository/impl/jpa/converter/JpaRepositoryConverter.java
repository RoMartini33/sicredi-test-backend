package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.converter;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.domain.VotoEnum;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.PautaJpaEntity;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.SessaoVotacaoJpaEntity;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.VotoJpaEntity;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.VotoJpaEnum;
import org.springframework.stereotype.Component;

@Component
public class JpaRepositoryConverter {

    public VotoJpaEntity convert(Voto voto) {
        if (voto == null) return null;
        VotoJpaEntity v = new VotoJpaEntity();

        v.setCodigo(voto.getCodigo());
        v.setCpf(voto.getCpf());
        v.setEscolha(convert(voto.getEscolha()));
        v.setPauta(convert(voto.getPauta()));
        v.setSessao(convert(voto.getSessao()));

        return v;
    }

    public Voto convert(VotoJpaEntity voto) {
        if (voto == null) return null;
        Voto v = new Voto();

        v.setCodigo(voto.getCodigo());
        v.setCpf(voto.getCpf());
        v.setEscolha(convert(voto.getEscolha()));
        v.setPauta(convert(voto.getPauta()));
        v.setSessao(convert(voto.getSessao()));

        return v;
    }

    public VotoJpaEnum convert(VotoEnum enunn) {
        return VotoJpaEnum.valueOf(enunn.name());
    }

    public VotoEnum convert(VotoJpaEnum enunn) {
        return VotoEnum.valueOf(enunn.name());
    }

    public PautaJpaEntity convert(Pauta pauta) {
        if (pauta == null) return null;
        PautaJpaEntity p = new PautaJpaEntity();

        p.setCodigo(pauta.getCodigo());
        p.setDataCriacao(pauta.getDataCriacao());
        p.setEmenta(pauta.getEmenta());
        p.setAssunto(pauta.getAssunto());

        return p;
    }

    public Pauta convert(PautaJpaEntity pauta) {
        if (pauta == null) return null;
        return Pauta.builder()
                .codigo(pauta.getCodigo())
                .criacao(pauta.getDataCriacao())
                .assunto(pauta.getAssunto())
                .ementa(pauta.getEmenta())
                .build();
    }

    public SessaoVotacao convert(SessaoVotacaoJpaEntity sv) {
        if (sv == null) return null;
        return SessaoVotacao.builder()
                .abertura(sv.getAbertura())
                .duracao(sv.getDuracao())
                .codigo(sv.getCodigo())
                .situacao(sv.getAtiva())
                .build();
    }

    public SessaoVotacaoJpaEntity convert(SessaoVotacao sv) {
        if (sv == null) return null;
        SessaoVotacaoJpaEntity svj = new SessaoVotacaoJpaEntity();

        svj.setCodigo(sv.getCodigo());
        svj.setAbertura(sv.getAbertura());
        svj.setFechamento(sv.getFechamento());
        svj.setDuracao(sv.getDuracao());
        svj.setAtiva(sv.situacao());

        return svj;
    }
}
