package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa;

import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.PautaJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PautaJpaRepository extends JpaRepository<PautaJpaEntity, Long> {

    Optional<PautaJpaEntity> findById(Long id);

}
