package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "voto")
public class VotoJpaEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long codigo;

    @Column(length = 3)
    @Enumerated(EnumType.STRING)
    private VotoJpaEnum escolha;

    @ManyToOne
    @JoinColumn(name="pauta_id")
    private PautaJpaEntity pauta;

    @ManyToOne
    @JoinColumn(name="sessao_id")
    private SessaoVotacaoJpaEntity sessao;

    private String cpf;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public VotoJpaEnum getEscolha() {
        return escolha;
    }

    public void setEscolha(VotoJpaEnum escolha) {
        this.escolha = escolha;
    }

    public PautaJpaEntity getPauta() {
        return pauta;
    }

    public void setPauta(PautaJpaEntity pauta) {
        this.pauta = pauta;
    }

    public SessaoVotacaoJpaEntity getSessao() {
        return sessao;
    }

    public void setSessao(SessaoVotacaoJpaEntity sessao) {
        this.sessao = sessao;
    }

    public String getCpf() {
        return StringUtils.trim(cpf);
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("escolha", escolha)
                .append("pauta", pauta)
                .append("sessao", sessao)
                .append("cpf", cpf)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        VotoJpaEntity that = (VotoJpaEntity) o;

        return new EqualsBuilder()
                .append(codigo, that.codigo)
                .append(escolha, that.escolha)
                .append(pauta, that.pauta)
                .append(sessao, that.sessao)
                .append(cpf, that.cpf)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(escolha)
                .append(pauta)
                .append(sessao)
                .append(cpf)
                .toHashCode();
    }
}

