package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa;

import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.SessaoVotacaoJpaEntity;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.VotoJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VotoJpaRepository extends JpaRepository<VotoJpaEntity, Long> {

    VotoJpaEntity findTopByPauta_CodigoAndSessao_CodigoAndCpf(long pCodigo, long sCodigo, String cpf);

    @Modifying
    @Query(value = "SELECT v.id, v.cpf, v.escolha, v.pauta_id, v.sessao_id FROM voto v WHERE v.pauta_id = :pautaId AND v.sessao_id = :sessaoId",
        nativeQuery = true
    )
    List<VotoJpaEntity> findAllByPautaAndAndSessao(@Param("pautaId") long codigoPauta,
                                          @Param("sessaoId") long codigoSessao);

    List<VotoJpaEntity> findAllBySessao(SessaoVotacaoJpaEntity sessao);

}
