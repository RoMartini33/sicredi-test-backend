package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity(name = "pauta")
public class PautaJpaEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long codigo;

    @Column(name = "dt_criacao", columnDefinition = "DATE")
    private LocalDate dataCriacao;

    private String assunto;
    private String ementa;


    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("assunto", assunto)
                .append("ementa", ementa)
                .append("dataCriacao", dataCriacao)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PautaJpaEntity that = (PautaJpaEntity) o;

        return new EqualsBuilder()
                .append(codigo, that.codigo)
                .append(dataCriacao, that.dataCriacao)
                .append(assunto, that.assunto)
                .append(ementa, that.ementa)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(dataCriacao)
                .append(assunto)
                .append(ementa)
                .toHashCode();
    }
}
