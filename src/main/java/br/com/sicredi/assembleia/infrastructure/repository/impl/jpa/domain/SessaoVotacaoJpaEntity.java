package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity(name = "sessao_votacao")
public class SessaoVotacaoJpaEntity implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long codigo;

    @Column(name = "dt_abertura", columnDefinition = "DATE")
    private LocalDateTime abertura;

    @Column(name = "dt_fechamento", columnDefinition = "DATE")
    private LocalDateTime fechamento;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name="pauta_id")
    private PautaJpaEntity pauta;

    private Integer duracao;
    private Boolean ativa;


    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public LocalDateTime getAbertura() {
        return abertura;
    }

    public void setAbertura(LocalDateTime abertura) {
        this.abertura = abertura;
    }

    public LocalDateTime getFechamento() {
        return fechamento;
    }

    public void setFechamento(LocalDateTime fechamento) {
        this.fechamento = fechamento;
    }

    public PautaJpaEntity getPauta() {
        return pauta;
    }

    public void setPauta(PautaJpaEntity pauta) {
        this.pauta = pauta;
    }

    public Integer getDuracao() {
        return duracao;
    }

    public void setDuracao(Integer duracao) {
        this.duracao = duracao;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("abertura", abertura)
                .append("fechamento", fechamento)
                .append("pauta", pauta)
                .append("duracao", duracao)
                .append("ativa", ativa)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SessaoVotacaoJpaEntity that = (SessaoVotacaoJpaEntity) o;

        return new EqualsBuilder()
                .append(codigo, that.codigo)
                .append(abertura, that.abertura)
                .append(fechamento, that.fechamento)
                .append(pauta, that.pauta)
                .append(duracao, that.duracao)
                .append(ativa, that.ativa)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(abertura)
                .append(fechamento)
                .append(pauta)
                .append(duracao)
                .append(ativa)
                .toHashCode();
    }
}
