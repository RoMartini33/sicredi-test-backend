package br.com.sicredi.assembleia.infrastructure.constraints;

public class InfrastructureException extends RuntimeException {
    private static final String INFRAESCTRUCTURE_EXCEPTION_MESSAGE
            = "Não foi possível processar sua operação no momento|";

    public InfrastructureException() {
        super(INFRAESCTRUCTURE_EXCEPTION_MESSAGE);
    }
}
