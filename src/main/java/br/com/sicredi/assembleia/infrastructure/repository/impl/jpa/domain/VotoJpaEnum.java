package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain;

import java.util.Arrays;

public enum VotoJpaEnum {
	SIM ("Sim"),
	NAO ("Não");

	private final String tipo;

	VotoJpaEnum(String tipo) {
		this.tipo = tipo;
	}

	public String getTipo() {
		return tipo;
	}
}
