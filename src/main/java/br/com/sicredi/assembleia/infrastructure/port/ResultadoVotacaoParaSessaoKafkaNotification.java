package br.com.sicredi.assembleia.infrastructure.port;

import br.com.sicredi.assembleia.domain.ResultadoVotacao;
import br.com.sicredi.assembleia.service.votacao.VotacaoAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ResultadoVotacaoParaSessaoKafkaNotification implements VotacaoAdapter {
    private Logger logger = LoggerFactory.getLogger(ResultadoVotacaoParaSessaoKafkaNotification.class);

    @Override
    public void notificar(ResultadoVotacao resultado) {

        //TODO: implementar notificação com KAFKA..

        logger.info("Notificando resultado de votação: {}", resultado);
    }
}
