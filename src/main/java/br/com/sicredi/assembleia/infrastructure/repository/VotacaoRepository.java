package br.com.sicredi.assembleia.infrastructure.repository;

import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.domain.Voto;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.VotoJpaRepository;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.converter.JpaRepositoryConverter;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.VotoJpaEntity;
import br.com.sicredi.assembleia.service.votacao.VotacaoDataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class VotacaoRepository implements VotacaoDataProvider {
    private VotoJpaRepository votoJpaRepository;
    private JpaRepositoryConverter repositoryConverter;

    @Autowired
    public VotacaoRepository(VotoJpaRepository votoJpaRepository, JpaRepositoryConverter repositoryConverter) {
        this.votoJpaRepository = votoJpaRepository;
        this.repositoryConverter = repositoryConverter;
    }

    @Override
    public Voto votar(Voto voto) {
        return repositoryConverter
                .convert(votoJpaRepository
                        .save(repositoryConverter.convert(voto)));
    }

    @Override
    public boolean votacaoExistente(Voto voto) {
        VotoJpaEntity votoJpaEntity =
                votoJpaRepository.findTopByPauta_CodigoAndSessao_CodigoAndCpf(voto.getPauta().getCodigo(),
                voto.getSessao().getCodigo(), voto.getCpf());

        return votoJpaEntity != null;
    }

    @Override
    public Optional<Collection<Voto>> buscarVotacoes(long codigoPauta, long codigoSessao) {
        List<VotoJpaEntity> votos =
                votoJpaRepository.findAllByPautaAndAndSessao(codigoPauta, codigoSessao);

        if (CollectionUtils.isEmpty(votos)) return Optional.ofNullable(Collections.emptyList());

        List<Voto> votacoes = votos.stream()
                .map(repositoryConverter::convert)
                .collect(Collectors.toList());

        return Optional.ofNullable(votacoes);
    }

    @Override
    public Optional<Collection<Voto>> buscarPorCodigoSessao(SessaoVotacao sessao) {
        List<VotoJpaEntity> votosEntities = votoJpaRepository.findAllBySessao(repositoryConverter.convert(sessao));
        if (CollectionUtils.isEmpty(votosEntities)) return Optional.ofNullable(Collections.emptyList());
        List<Voto> votos = votosEntities.stream().map(repositoryConverter::convert).collect(Collectors.toList());

        return Optional.ofNullable(votos);
    }

    @Override
    public Optional<Voto> buscarPorCodigo(long codigoSessao) {
        VotoJpaEntity votoJpaEntity = votoJpaRepository.findById(codigoSessao).orElse(null);
        if (votoJpaEntity == null) return Optional.empty();

        return Optional.ofNullable(repositoryConverter.convert(votoJpaEntity));
    }


}
