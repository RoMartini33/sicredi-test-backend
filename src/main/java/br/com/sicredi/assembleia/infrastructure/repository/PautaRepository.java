package br.com.sicredi.assembleia.infrastructure.repository;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.PautaJpaRepository;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.converter.JpaRepositoryConverter;
import br.com.sicredi.assembleia.service.pauta.PautaDataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PautaRepository implements PautaDataProvider {
    private PautaJpaRepository pautaRepository;
    private JpaRepositoryConverter converter;

    @Autowired
    public PautaRepository(PautaJpaRepository pautaRepository, JpaRepositoryConverter jpaRepositoryConverter) {
        this.pautaRepository = pautaRepository;
        this.converter = jpaRepositoryConverter;
    }

    @Override
    public Pauta adicionar(Pauta pauta) {
        return converter.convert(pautaRepository.save(converter.convert(pauta)));
    }

    @Override
    public Optional<Pauta> buscarPorCodigo(long codigo) {
        return Optional.ofNullable(converter.convert(
                pautaRepository.findById(codigo)
                        .orElse(null)));
    }
}
