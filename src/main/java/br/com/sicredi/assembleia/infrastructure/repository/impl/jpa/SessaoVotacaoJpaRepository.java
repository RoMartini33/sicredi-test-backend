package br.com.sicredi.assembleia.infrastructure.repository.impl.jpa;

import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.SessaoVotacaoJpaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface SessaoVotacaoJpaRepository extends JpaRepository<SessaoVotacaoJpaEntity, Long> {

    List<SessaoVotacaoJpaEntity> findByAtiva(Boolean situacao);

    List<SessaoVotacaoJpaEntity> findAllByPauta(Long pautaId);

    @Modifying
    @Query("update sessao_votacao sv set sv.ativa = :situacao where sv.codigo = :id")
    @Transactional
    void closeSession(@Param("situacao") boolean situacao,  @Param("id") Long id);

}
