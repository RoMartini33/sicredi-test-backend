package br.com.sicredi.assembleia.infrastructure.repository;

import br.com.sicredi.assembleia.domain.Pauta;
import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.PautaJpaRepository;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.SessaoVotacaoJpaRepository;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.converter.JpaRepositoryConverter;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.PautaJpaEntity;
import br.com.sicredi.assembleia.infrastructure.repository.impl.jpa.domain.SessaoVotacaoJpaEntity;
import br.com.sicredi.assembleia.service.sessao.SessaoVotacaoDataProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class SessaoVotacaoRepository implements SessaoVotacaoDataProvider {
    private Logger logger = LoggerFactory.getLogger(SessaoVotacaoRepository.class);

    private SessaoVotacaoJpaRepository sessaoJpaRepository;
    private PautaJpaRepository pautaJpaRepository;
    private JpaRepositoryConverter jpaRepositoryConverter;

    @Autowired
    public SessaoVotacaoRepository(SessaoVotacaoJpaRepository votacaoJpaRepository, PautaJpaRepository pautaJpaRepository,
                                   JpaRepositoryConverter jpaRepositoryConverter) {
        this.sessaoJpaRepository = votacaoJpaRepository;
        this.pautaJpaRepository = pautaJpaRepository;
        this.jpaRepositoryConverter = jpaRepositoryConverter;
    }

    @Override
    public SessaoVotacao iniciar(Pauta pauta, SessaoVotacao sessaoVotacao) {
        PautaJpaEntity pautaJpaEntity = pautaJpaRepository.findById(pauta.getCodigo()).get();

        SessaoVotacaoJpaEntity sv = new SessaoVotacaoJpaEntity();
        sv.setAtiva(sessaoVotacao.situacao());
        sv.setAbertura(sessaoVotacao.getAbertura());
        sv.setDuracao(sessaoVotacao.getDuracao());
        sv.setFechamento(sessaoVotacao.getFechamento());
        sv.setPauta(pautaJpaEntity);

        SessaoVotacaoJpaEntity saved = sessaoJpaRepository.save(sv);

        return SessaoVotacao.builder()
                .abertura(saved.getAbertura())
                .duracao(saved.getDuracao())
                .codigo(saved.getCodigo())
                .situacao(saved.getAtiva())
                .build();
    }

    @Override
    public void finalizarSessao(SessaoVotacao sessao) {
        sessaoJpaRepository.closeSession(false, sessao.getCodigo());
    }

    @Override
    public Optional<Collection<SessaoVotacao>> buscarSessoesAtivas() {
        List<SessaoVotacaoJpaEntity> sessoes = sessaoJpaRepository.findByAtiva(true);
        return getSessoes(sessoes);

    }

    @Override
    public Optional<Collection<SessaoVotacao>> buscarSessoesParaPauta(Pauta pauta) {
        List<SessaoVotacaoJpaEntity> sessoes = sessaoJpaRepository.findAllByPauta(pauta.getCodigo());
        return getSessoes(sessoes);
    }

    @Override
    public Optional<SessaoVotacao> buscarSessaoPorCodigo(long codigo) {
        SessaoVotacaoJpaEntity sessao = sessaoJpaRepository.findById(codigo).orElse(null);

        if (sessao == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(jpaRepositoryConverter.convert(sessao));
    }

    private Optional<Collection<SessaoVotacao>> getSessoes(List<SessaoVotacaoJpaEntity> sessoes) {
        if (sessoes == null) {
            return Optional.empty();
        }

        List<SessaoVotacao> sessoesVotacao = sessoes.stream()
                .map(s -> jpaRepositoryConverter.convert(s))
                .collect(Collectors.toList());

        return Optional.ofNullable(sessoesVotacao);
    }

}
