package br.com.sicredi.assembleia.infrastructure.adapter;

import br.com.sicredi.assembleia.domain.SessaoVotacao;
import br.com.sicredi.assembleia.service.sessao.FinalizacaoSessaoVotacaoAdapter;
import br.com.sicredi.assembleia.service.votacao.VotacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SessaoVotacaoNotification implements FinalizacaoSessaoVotacaoAdapter {
    private VotacaoService votacaoService;

    @Autowired
    public SessaoVotacaoNotification(VotacaoService votacaoService) {
        this.votacaoService = votacaoService;
    }

    @Override
    public void notificarFinalizacaoDaSessao(SessaoVotacao sessao) {
        votacaoService.gerarResultadoParaVotacao(sessao);
    }
}
