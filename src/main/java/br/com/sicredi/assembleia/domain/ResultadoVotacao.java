package br.com.sicredi.assembleia.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ResultadoVotacao {
    private long totalVotosFavoraveis;
    private long totalVotosContrarios;
    private long codigoSessao;
    private long codigoPauta;
    private ResultadoVotacaoEnum situacao;

    private ResultadoVotacao(final long totalVotosFavoraveis, final long totalVotosContrarios, final long sessaoId, final long pautaId) {
        this.totalVotosFavoraveis = totalVotosFavoraveis;
        this.totalVotosContrarios = totalVotosContrarios;
        this.codigoSessao = sessaoId;
        this.codigoPauta = pautaId;
        this.situacao = determinarSituacao();
    }

    private ResultadoVotacaoEnum determinarSituacao() {
        if (getTotalVotosFavoraveis() > getTotalVotosContrarios())
            return ResultadoVotacaoEnum.APROVADA;

        if (getTotalVotosFavoraveis() < getTotalVotosContrarios())
            return ResultadoVotacaoEnum.REJEITADA;

        return ResultadoVotacaoEnum.EMPATE;
    }

    public long getTotalVotosFavoraveis() {
        return totalVotosFavoraveis;
    }

    public long getTotalVotosContrarios() {
        return totalVotosContrarios;
    }

    public ResultadoVotacaoEnum getSituacao() {
        return situacao;
    }

    public long getCodigoSessao() {
        return codigoSessao;
    }

    public long getCodigoPauta() {
        return codigoPauta;
    }


    public static ResultadoVotacaoBuilder builder() {
        return new ResultadoVotacaoBuilder();
    }

    public static class ResultadoVotacaoBuilder {
        private long votosFavoraveis;
        private long votosContrarios;
        private long sessaoId;
        private long pautaId;

        public ResultadoVotacaoBuilder votosFavoraveis(final long totalVotosFavoraveis) {
            this.votosFavoraveis = totalVotosFavoraveis;
            return this;
        }

        public ResultadoVotacaoBuilder votosContrarios(final long totalVotosContrarios) {
            this.votosContrarios = totalVotosContrarios;
            return this;
        }

        public ResultadoVotacaoBuilder codigoPauta(final long codigoPauta) {
            this.pautaId = codigoPauta;
            return this;
        }

        public ResultadoVotacaoBuilder codigoSessao(final long codigoSessao) {
            this.sessaoId = codigoSessao;
            return this;
        }

        public ResultadoVotacao build() {
            return new ResultadoVotacao(votosFavoraveis, votosContrarios, sessaoId, pautaId);
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ResultadoVotacao that = (ResultadoVotacao) o;

        return new EqualsBuilder()
                .append(totalVotosFavoraveis, that.totalVotosFavoraveis)
                .append(totalVotosContrarios, that.totalVotosContrarios)
                .append(codigoSessao, that.codigoSessao)
                .append(codigoPauta, that.codigoPauta)
                .append(situacao, that.situacao)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(totalVotosFavoraveis)
                .append(totalVotosContrarios)
                .append(codigoSessao)
                .append(codigoPauta)
                .append(situacao)
                .toHashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ResultadoVotacao{");
        sb.append("totalVotosFavoraveis=").append(totalVotosFavoraveis);
        sb.append(", totalVotosContrarios=").append(totalVotosContrarios);
        sb.append(", codigoSessao=").append(codigoSessao);
        sb.append(", codigoPauta=").append(codigoPauta);
        sb.append(", situacao=").append(situacao);
        sb.append('}');
        return sb.toString();
    }
}
