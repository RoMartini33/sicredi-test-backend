package br.com.sicredi.assembleia.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;

public class Pauta {
    private Long codigo;
    private String assunto;
    private String ementa;
    private LocalDate dataCriacao;

    private Pauta(Long codigo, String assunto, String ementa, LocalDate dataCriacao, SessaoVotacao sessao) {
        this.codigo = codigo;
        this.assunto = assunto;
        this.ementa = ementa;
        this.dataCriacao = dataCriacao != null ? dataCriacao : LocalDate.now();
    }

    public Long getCodigo() {
        return codigo;
    }

    public String getAssunto() {
        return assunto;
    }

    public String getEmenta() {
        return ementa;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Pauta pauta = (Pauta) o;

        return new EqualsBuilder()
                .append(codigo, pauta.codigo)
                .append(assunto, pauta.assunto)
                .append(ementa, pauta.ementa)
                .append(dataCriacao, pauta.dataCriacao)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(assunto)
                .append(ementa)
                .append(dataCriacao)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("assunto", assunto)
                .append("ementa", ementa)
                .append("dataCriacao", dataCriacao)
                .toString();
    }

    public static PautaBuilder builder() {
        return new PautaBuilder();
    }

    public static class PautaBuilder {
        private Long codigo;
        private String assunto;
        private String ementa;
        private LocalDate dataCriacao;
        private SessaoVotacao sessao;

        public PautaBuilder codigo(final long codigo) {
            this.codigo = codigo;
            return this;
        }

        public PautaBuilder assunto(String assunto) {
            this.assunto = assunto;
            return this;
        }

        public PautaBuilder ementa(String ementa) {
            this.ementa = ementa;
            return this;
        }

        public PautaBuilder criacao(final LocalDate dataCriacao) {
            this.dataCriacao = dataCriacao;
            return this;
        }

        public PautaBuilder sessao(SessaoVotacao sessao) {
            this.sessao = sessao;
            return this;
        }

        public Pauta build() {
            return new Pauta(codigo, assunto, ementa, dataCriacao, sessao);
        }
    }
}
