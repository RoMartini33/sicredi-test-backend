package br.com.sicredi.assembleia.domain;

public enum ResultadoVotacaoEnum {
    APROVADA,
    REJEITADA,
    EMPATE
}
