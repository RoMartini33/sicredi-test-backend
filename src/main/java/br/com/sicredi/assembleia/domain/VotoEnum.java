package br.com.sicredi.assembleia.domain;

import java.util.Arrays;

public enum VotoEnum {
    SIM (true),
    NAO (false);

    private static final String UNSUPPORTED_TYPE_MESSAGE = "Opção de voto não suportada!";
    private boolean opcao;

    VotoEnum(boolean opcao) {
        this.opcao = opcao;
    }

    public boolean getOpcao() {
        return opcao;
    }

    public static VotoEnum getByValue(final boolean opcao) {
        return Arrays.stream(VotoEnum.values())
                .filter(type -> type.opcao == opcao)
                .findFirst()
                .orElseThrow(() ->
                        new IllegalStateException(String.format(UNSUPPORTED_TYPE_MESSAGE, opcao)));
    }

}
