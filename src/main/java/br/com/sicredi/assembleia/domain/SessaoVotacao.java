package br.com.sicredi.assembleia.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;

public class SessaoVotacao {
    private static final int DURACAO_PADRAO_EM_MINUTOS = 1;
    private Long codigo;
    private LocalDateTime abertura;
    private LocalDateTime fechamento;
    private Integer duracao;
    private Boolean ativa;

    private SessaoVotacao(Long codigo, LocalDateTime abertura, Integer duracao, Boolean ativa) {
        this.codigo = codigo;
        this.abertura = abertura;
        this.duracao = duracao;
        determinaDataFechamento();
        this.ativa = ativa;
    }

    private void determinaDataFechamento() {
        fechamento = getAbertura()
                .plusMinutes(getDuracao());
    }

    private int determinarDuracaoEmMinutos() {
        return duracao == null ? DURACAO_PADRAO_EM_MINUTOS : duracao;
    }

    public Long getCodigo() {
        return codigo;
    }

    public LocalDateTime getAbertura() {
        return abertura;
    }

    public LocalDateTime getFechamento() {
        return fechamento;
    }

    public Integer getDuracao() {
        return determinarDuracaoEmMinutos();
    }

    public Boolean situacao() {
        return ativa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        SessaoVotacao that = (SessaoVotacao) o;

        return new EqualsBuilder()
                .append(codigo, that.codigo)
                .append(abertura, that.abertura)
                .append(fechamento, that.fechamento)
                .append(duracao, that.duracao)
                .append(ativa, that.ativa)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(abertura)
                .append(fechamento)
                .append(duracao)
                .append(ativa)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("abertura", abertura)
                .append("fechamento", fechamento)
                .append("duracao", duracao)
                .append("ativa", ativa)
                .toString();
    }

    public static SessaoVotacaoBuilder builder() {
        return new SessaoVotacaoBuilder();
    }

    public static class SessaoVotacaoBuilder {
        private Long codigo;
        private LocalDateTime abertura;
        private Integer duracao;
        private boolean situacao;

        public SessaoVotacaoBuilder codigo(long codigo) {
            this.codigo = codigo;
            return this;
        }

        public SessaoVotacaoBuilder abertura(LocalDateTime abertura) {
            this.abertura = abertura;
            return this;
        }

        public SessaoVotacaoBuilder duracao(int duracao) {
            this.duracao = duracao;
            return this;
        }

        public SessaoVotacaoBuilder situacao(boolean situacao) {
            this.situacao = situacao;
            return this;
        }

        public SessaoVotacao build() {
            return new SessaoVotacao(codigo, abertura, duracao, situacao);
        }
    }
}
