package br.com.sicredi.assembleia.domain;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Voto {
    private Long codigo;
    private String cpf;
    private Pauta pauta;
    private SessaoVotacao sessao;
    private VotoEnum escolha;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getCpf() {
        return StringUtils.trim(cpf);
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Pauta getPauta() {
        return pauta;
    }

    public void setPauta(Pauta pauta) {
        this.pauta = pauta;
    }

    public SessaoVotacao getSessao() {
        return sessao;
    }

    public void setSessao(SessaoVotacao sessao) {
        this.sessao = sessao;
    }

    public VotoEnum getEscolha() {
        return escolha;
    }

    public void setEscolha(VotoEnum escolha) {
        this.escolha = escolha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Voto voto = (Voto) o;

        return new EqualsBuilder()
                .append(codigo, voto.codigo)
                .append(cpf, voto.cpf)
                .append(pauta, voto.pauta)
                .append(sessao, voto.sessao)
                .append(escolha, voto.escolha)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(codigo)
                .append(cpf)
                .append(pauta)
                .append(sessao)
                .append(escolha)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("codigo", codigo)
                .append("cpf", cpf)
                .append("pauta", pauta)
                .append("sessao", sessao)
                .append("escolha", escolha)
                .toString();
    }
}
