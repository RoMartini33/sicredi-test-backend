CREATE TABLE IF NOT EXISTS `voto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cpf` CHAR(11) NOT NULL,
  `escolha` varchar(3) DEFAULT NULL,
  `pauta_id` bigint(20) DEFAULT NULL,
  `sessao_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
