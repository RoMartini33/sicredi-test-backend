CREATE TABLE IF NOT EXISTS `pauta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dt_criacao` date DEFAULT NULL,
  `assunto` varchar(255) DEFAULT NULL,
  `ementa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;