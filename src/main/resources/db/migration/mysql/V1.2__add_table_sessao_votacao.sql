CREATE TABLE IF NOT EXISTS `sessao_votacao` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dt_abertura` datetime DEFAULT NULL,
  `dt_fechamento` datetime DEFAULT NULL,
  `ativa` BOOLEAN DEFAULT 0,
  `duracao` int(10) DEFAULT NULL,
  `pauta_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
